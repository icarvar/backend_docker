let Server =  require('./lib/server');
const options = {
  port: process.env.PORT || 3000
};

Server.start(options, (error) => {
  if (!error){
    Server.loadComponents();
    console.log('The server is running....');
  }
});