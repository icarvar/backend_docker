const gulp = require('gulp');

const eslint = require('gulp-eslint'); 

gulp.task('lint', () => { 
  return gulp.src(['lib/**/*.js'])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

const jasmine = require('gulp-jasmine'); 
gulp.task('test', () =>
    gulp.src('lib/**/*.spec.js')
        // gulp-lepaths so you can't have any plugins before it 
        .pipe(jasmine())
);

const istanbul = require('gulp-istanbul');
gulp.task('pre-test', function () {
  return gulp.src(['./lib/**/*.controller.js'])
    // Covering files
    .pipe(istanbul())
    // Force `require` to return covered files
    .pipe(istanbul.hookRequire());
});

gulp.task('code-coverage', ['pre-test'], function () {
  return gulp.src(['./lib/**/*.spec.js'])
    .pipe(jasmine())
    // Creating the reports after tests ran
    .pipe(istanbul.writeReports())
    // Enforce a coverage of at least 90%
    .pipe(istanbul.enforceThresholds({thresholds:{global: 80}}));
});


gulp.task('default', function() {
  console.log('Hello world by GULP');
});