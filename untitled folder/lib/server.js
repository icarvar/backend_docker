let http = require('http');

let Express = require('express');

let bodyParser = require('body-parser');

let app = Express();


var Server = {
  start : function (options, done) {
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(bodyParser.json());
    let server = http.createServer(app);
    server.listen(options.port, done);
  },
  loadComponents : function() {
        // @TODO make load of components
    let taskModule = require('./components/main');
    taskModule(app);
  }
};

module.exports = Server;