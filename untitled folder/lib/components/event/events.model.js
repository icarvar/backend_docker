var eventList = [];

var Event = {

  create : function(event){
    event.id = eventList.length + 1;
    eventList.push(event);
    return Promise.resolve(event);
  },
  getAll : function(){
    return Promise.resolve(eventList);
  }
};

module.exports = Event;