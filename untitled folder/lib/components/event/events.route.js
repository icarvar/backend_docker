const express = require('express');
var Events = require('./events.controller');

var router = express.Router();

router.post('/',Events.post);
router.get('/',Events.getAllEvents);

module.exports = router;