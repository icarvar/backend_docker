var Event = require('./events.model');

function post(req, res){

  return Event.create(req.body)
  .then(event => res.send(event));
}

function getAllEvents(req, res){
  
  return Event.getAll()
  .then(events => res.send(events));
}

module.exports = {post, getAllEvents};