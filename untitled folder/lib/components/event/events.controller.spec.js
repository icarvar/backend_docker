const mockery= require('mockery');

let eventsController;

const EventModelMock = {
  getAll(){},
  create(){}
};


describe('Event controller.', function () {

  beforeEach(function(){
    
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });

    mockery.registerMock('./events.model', EventModelMock);

    eventsController = require('./events.controller');

  });

  describe('GetAllEvents', function(){
    it('Should be get all event',function(done){
      var res = {
        send(){}
      };

      spyOn(EventModelMock, 'getAll').and.returnValue(Promise.resolve({
        id : 2,
        title : 'Buss'
      }));

      spyOn(res,'send');

      eventsController.getAllEvents({},res)
      .then(function(){
        expect(EventModelMock.getAll).toHaveBeenCalled();
        expect(res.send).toHaveBeenCalled();
        expect(res.send).toHaveBeenCalledWith({id : 2, title : 'Buss'});
        done();
      });
      
    });

  });

  describe('PostEvent', function(){
    it('Should be create an event',function(done){
      var res = {
        send(){}
      };

      var req = {
        body : {
          id : 1,
          title : 'Buss'
        }
      };

      spyOn(EventModelMock, 'create').and.returnValue(Promise.resolve({
        id : 1,
        title : 'Buss'
      }));

      spyOn(res,'send');

      eventsController.post(req,res)
      .then(function(){
        expect(EventModelMock.create).toHaveBeenCalled();
        expect(res.send).toHaveBeenCalled();
        expect(res.send).toHaveBeenCalledWith({id : 1, title : 'Buss'});
        done();
      });
      
    });

  });

});