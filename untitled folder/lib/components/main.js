const express = require('express');

const bodyParser = require('body-parser');

var router = require('./event/events.route');

module.exports = function(app){
  //add here more end points.
  app.use('/events', router);

};