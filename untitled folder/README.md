README SERVER
=============
This is the back-end for ticket-manger.

PROGRAMS
============
You need the following programs:
* node v6.9.4 or up. 
* docker v1.2.5 or up.

Just for ur knowing, the program is using :
* eslint.
* body-parser.
* express.
* gulp.
* gulp-eslint.
* gulp-istanbul.
* gulp-jasmine.

That you would see in the package.son of the project.

COMMANDS
========
In the shell go to project  ./ticket-manager-back-end, and type the command:

* npm install

and then

* node index.js

Yo should see the message:
      
      The server is running....

HOW IT WORKS
============

Go to browser, and type:

http://localhost/3000





